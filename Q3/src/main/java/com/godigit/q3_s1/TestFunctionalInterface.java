package com.godigit.q3_s1;
@FunctionalInterface
interface Greetings {
	public void goodMorning();
	// this is an functional interface (which has only one method).
}
public class TestFunctionalInterface {

	public static void main(String[] args) {
		Greetings gm=()->System.out.println("good morning! have a nice day!");
		// i have used lambda expression to define the method of FI.
		gm.goodMorning();

	}

}
