package com.godigit.q3_s1;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class TestStream {

	public static void main(String[] args) {
		//boys list 
		List<String> boys = Arrays.asList("ARJUN","ADITHYA","DEEPAK","RONITH");
		//girls list 
		List<String> girls = Arrays.asList("SUSHMITHA","NAMRUTHA","BOOMIKA","KIRAN","TANYA","MAHI");
		//list of students->stream
		Stream<String> students = Stream.concat(boys.stream(), girls.stream());
	    //displaying the elements of the concatenated stream
		students.forEach(str->System.out.print(str+" , "));
	}

}
