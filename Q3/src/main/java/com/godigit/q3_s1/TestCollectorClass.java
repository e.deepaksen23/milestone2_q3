package com.godigit.q3_s1;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

class Expense{
	private int	s_no;
	private String expenditure;
	float amount;
	public Expense(int s_no, String expenditure, float amount) {
		super();
		this.s_no = s_no;
		this.expenditure = expenditure;
		this.amount = amount;
	}
	
}
public class TestCollectorClass {

	public static void main(String[] args) {
		List<Expense> expenseList = new ArrayList<Expense>();
		expenseList.add(new Expense(1, "RENT", 8000f));
		expenseList.add(new Expense(2, "FOOD", 400f));
		expenseList.add(new Expense(3, "CLOTHES", 2500f));
		expenseList.add(new Expense(4, "WATER", 2000f));
		expenseList.add(new Expense(5, "ELECTRICITY", 2000f));
		//this is the list containing the expenses.
		
		List<Float> expenseAmountList = expenseList.stream().map(x->x.amount).collect(Collectors.toList());
		System.out.println("THIS IS THE LIST OF ALL EXPENSE AMOUNT \n"+expenseAmountList);
		//this prints the list of all expenses
		Double totalExpense = expenseList.stream().collect(Collectors.summingDouble(x->x.amount)); 
		System.out.println("TOTAL EXPENSE : "+totalExpense+" rs");
		//this prints the sum of all expenses
		long noOfExpense = expenseList.stream().collect(Collectors.counting()); 
		System.out.println("TOTAL NUMBER OF EXPENSE : "+noOfExpense);
		//this prints the count of all expenses
		Double avgExpense = expenseList.stream().collect(Collectors.averagingDouble(x->x.amount)); 
		System.out.println("AVERAGE EXPENSE : "+avgExpense+" rs");
		//this prints the average of all expenses
		
	}

}
