package com.godigit.q3_s1;

import java.util.ArrayList;
import java.util.List;

public class TestStreamFilter {

	public static void main(String[] args) {
		List<String> days = new ArrayList<String>();
		days.add("MONDAY");
		days.add("TUESDAY");
		days.add("WEDNESDAY");
		days.add("THURSDAY");
		days.add("FRIDAY");
		days.add("SATURDAY");
		days.add("SUNDAY");
		//this is the list of all days of a week
		long count = days.stream().filter(str->str.startsWith("T")).count();
		//this line is used to count the number of days which starts with letter "T"
		System.out.println("There are "+count+" days which starts with letter 'T'");

			

	}

}
