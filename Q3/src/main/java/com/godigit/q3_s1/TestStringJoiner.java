package com.godigit.q3_s1;
import java.util.StringJoiner;
public class TestStringJoiner {

	public static void main(String[] args) {
		StringJoiner string = new StringJoiner(" ","[","]");
		//object of string joiner class is created.
		string.setEmptyValue("this string is empty");//setting default value which willbe
		//displayed when string is empty.
		System.out.println(string);
		
		//adding values to string
		string.add("deepak").add("sen").add("e");
		System.out.println(string);
		//creating another stringjoiner object
		StringJoiner str = new StringJoiner(" ","pre","suf");
		str.add("is a").add("DIGIT").add("employee");
		
		string.merge(str);// string=string+str
		
		System.out.println(string);
		System.out.println("length of final string is :"+string.length());
	}

}
