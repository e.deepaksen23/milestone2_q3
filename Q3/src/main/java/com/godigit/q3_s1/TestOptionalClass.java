package com.godigit.q3_s1;
import java.util.Optional;  
public class TestOptionalClass {
	public static void main(String [] args) {
		String[] str = new String[6];        
        str[0] = null;
        str[1] = "pass";
        str[2] = "fail";
        str[3] = null;
        str[4] = "pass";
        str[5] = "fail";
        //an array of strings is declared with results as "pass/fail" or null
        int resultDeclared=0;
        int resultWithHeld=0;
        int pass=0;
        int fail=0;
      for (int k=0;k<str.length;k++) {
      Optional<String> availability = Optional.ofNullable(str[k]);
      
      if(availability.isPresent())//checks whether string is not null
      {resultDeclared++;
      if(str[k].equals("pass")) {pass++;}
      else {fail++;}
      }
      else
      {resultWithHeld++;}
      
      }
      System.out.println(" NO OF RESULTS WITHELD : "+resultWithHeld);
      System.out.println(" NO OF RESULTS DECLARED : "+resultDeclared);
      System.out.println(" \t NO OF PASS : "+pass);
      System.out.println(" \t NO OF FAIL : "+fail);

	}
	

}
