package com.godigit.q3_s1;
@FunctionalInterface
interface Area {
	public float area(int len, int bth);
	// this is an functional interface (which has only one method).
}
public class TestLambdaExpression {

	public static void main(String[] args) {
		Area a = (l,b)->l*b;
	//this is the lambda expression used to define the function of method of functional interface
		System.out.println("AREA OF RECTANGLE : "+a.area(5, 10));
	}

}

