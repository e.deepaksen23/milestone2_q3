package com.godigit.q3_s1;
//Method reference to an instance method of an object
@FunctionalInterface
interface Language {
	// this is an functional interface (which has only one method).
	public void speaking();
	
}
public class TestMethodReference {
	public void Sound() {
		System.out.println(" I SPEAK TAMIL");
		// this is method defined inside 'TestMethodReference' class.
	}

	public static void main(String[] args) {
		TestMethodReference tmr = new TestMethodReference();
		// method of FI is defined using method reference.
		Language lang = tmr::Sound;// Method reference using the object of the class
		lang.speaking();// Calling the method of functional interface 
		

	}

}
