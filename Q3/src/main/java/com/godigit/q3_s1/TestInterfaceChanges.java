package com.godigit.q3_s1;

interface MajorInterface{
	default void DefaultMethod() {
		System.out.println("THIS IS THE DEFAULT METHOD!");
	}
	static void StaticMethod() {
		System.out.println("THIS IS THE STATIC METHOD!");
	}
	public void NormalMethod();
}
public class TestInterfaceChanges implements MajorInterface{
	@Override
	public void NormalMethod() {
		System.out.println("THIS IS THE NORMAL METHOD!");
		
	}
	public static void main(String[] args) {
		TestInterfaceChanges tic =new TestInterfaceChanges();
		tic.DefaultMethod();// default method is defined inside interface , called using obj of implmented class
		tic.NormalMethod();// normal method is defined implemented class , called using obj of implmented class
		MajorInterface.StaticMethod();// static method is defined inside interface , called directly using interface
		
	}

	
}
