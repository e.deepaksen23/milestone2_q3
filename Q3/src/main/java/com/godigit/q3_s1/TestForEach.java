package com.godigit.q3_s1;

import java.util.ArrayList;
import java.util.List;

public class TestForEach {
	public static void main(String[] args) {
		List<Integer> num = new ArrayList<Integer>();
	      num.add(1);
	      num.add(2);
	      num.add(3);
	      num.add(4); 
	      num.add(5);
	      //this is a list of integers.
	      System.out.println("numbers");
	      num.forEach(str->System.out.print(str+" " ));
	      List<Integer> squares = new ArrayList<Integer>();
	      num.forEach(i->squares.add(i*i));
	      //this is used to create a list of squares of previous list.
	      System.out.println("\nsquares");
	      squares.forEach(str->System.out.print(str+" " ));
	}
}
